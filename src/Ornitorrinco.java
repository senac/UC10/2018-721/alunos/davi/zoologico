
public class Ornitorrinco implements Mamífero, Ave {

    @Override
    public void mamar() {
        System.out.println("Mamando");
    }

    @Override
    public void falar() {
        System.out.println("Falando");
    }

    @Override
    public void andar() {
        System.out.println("Andando");
    }

    @Override
    public void poeOvo() {
        System.out.println("Botando ovo");
    }

}
